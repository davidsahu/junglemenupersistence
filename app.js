const yargs = require("yargs");
const commands = require("./commands");

let command = yargs.argv._[0];
let animal = yargs.argv.animal;
let energy = yargs.argv.energy;
let food = yargs.argv.food;
let animalType = yargs.argv.animalType;

switch (command) {
  case "add":
    if (animal && energy) {
      commands.add(animal, energy);
    } else {
      console.log("enter animal and energy");
    }
    break;
  case "eat":
    if (animal && food) {
      commands.eat(animal, food);
    } else {
      console.log("need to specify animal and food");
    }
    break;
  case "play":
    if (animal) {
      commands.play(animal);
    } else {
      console.log("need to specify animal");
    }
    break;

  case "sleep":
    if (animal) {
      commands.sleep(animal);
    } else {
      console.log("need to specify animal");
    }
    break;

  case "sound":
    if (animal) {
      commands.sound(animal);
    } else {
      console.log("need to specify animal");
    }
    break;

  case "list":
    commands.printList(animalType);
    break;

  case "instructions":
    commands.instructions();
}

// if (command === "add") {
//   if (animal && energy) {
//     commands.add(animal, energy);
//   } else {
//     console.log("find error");
//   }
// } else if (command === "eat") {
//   if (animal && food) {
//     commands.eat(animal, food);
//   } else {
//     console.log("need to specify animal and food");
//   }
// } else if (command === "play") {
//   if (animal) {
//     commands.play(animal);
//   } else {
//     console.log("need to specify animal");
//   }
// } else if (command === "sleep") {
//   if (animal) {
//     commands.sleep(animal);
//   } else {
//     console.log("need to specify animal");
//   }
// } else if (command === "sound") {
//   if (animal) {
//     commands.sound(animal);
//   } else {
//     console.log("need to specify animal");
//   }
// } else if (command === "list") {
//   commands.printList(animalType);
// }
