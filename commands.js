const fs = require("fs");

const readFile = () => {
  try {
    return JSON.parse(fs.readFileSync("animalList.json"));
  } catch (error) {
    return [];
  }
};

var animalList = [];

try {
  animalList = JSON.parse(fs.readFileSync("animalList.json"));
} catch (e) {
  console.log(e);
}

class Animal {
  constructor(food, animal) {
    this.food = food;
    this.animal = animal;
  }

  eat() {
    console.log(`${this.name} im eating and gaining points`);
  }

  play() {
    console.log(`${this.name} i dont know what to do`);
  }

  sleep() {
    console.log(`${this.name} im sleeping`);
  }

  sound() {
    console.log("making sound");
  }
}

class Tiger extends Animal {
  constructor(food, animal) {
    super(food, animal);
  }
  eat() {
    if (this.food === "grain") {
      console.log("I cant eat that :( remember my sensitive digestive system");
    } else {
      var index = animalList.findIndex((x) => x.animal === this.animal);
      console.log(`Tiger Recovering +5 energy`);
      animalList[index].energy += 5;
    }
  }
  sleep() {
    var index = animalList.findIndex((x) => x.animal === this.animal);
    animalList[index].energy += 5;
    console.log(
      `Tiger gains +5 of energy is sleeping, now has ${animalList[index].energy} of energy `
    );
  }
  sound() {
    var index = animalList.findIndex((x) => x.animal === this.animal);

    if (animalList[index].energy >= 3) {
      animalList[index].energy -= 3;
      console.log(`Tiger say raaawr raaawr`);
      console.log(
        `Tiger use -3 of energy, now has ${animalList[index].energy} of energy `
      );
    } else {
      console.log(
        `Tiger dont have enough energy,  has ${animalList[index].energy} of energy `
      );
    }
  }
}

class Monkey extends Animal {
  constructor(food, animal) {
    super(food, animal);
  }
  eat() {
    var index = animalList.findIndex((x) => x.animal === this.animal);
    console.log(`Monkey Recovering +2 energy`);
    console.log(index);
    animalList[index].energy += 2;
  }
  play() {
    var index = animalList.findIndex((x) => x.animal === this.animal);
    if (animalList[index].energy >= 8) {
      console.log(`Monkey scream Oooo Oooo! and is playing `);
      console.log(`Mokey lost -8 of energy`);
      animalList[index].energy -= 8;
    } else {
      console.log(`I’m too tired, i need rest or eat`);
    }
  }
  sleep() {
    var index = animalList.findIndex((x) => x.animal === this.animal);
    animalList[index].energy += 10;
    console.log(
      `Monkey gains +10 is sleeping, now has ${animalList[index].energy} of energy `
    );
  }

  sound() {
    var index = animalList.findIndex((x) => x.animal === this.animal);

    if (animalList[index].energy >= 3) {
      animalList[index].energy -= 3;
      console.log(`Monkey say Oooo Oooo`);
      console.log(
        `Monkey use -3 of energy, now has ${animalList[index].energy} of energy `
      );
    } else {
      console.log(
        `Monkey dont have enough energy,  has ${animalList[index].energy} of energy `
      );
    }
  }
}

const add = (animal, energy) => {
  try {
    animalList = JSON.parse(fs.readFileSync("animalList.json"));
  } catch (e) {
    console.log(e);
  }
  let index = animalList.findIndex((x) => x.animal === animal);

  if (index === -1) {
    animalList.push({ animal, energy });
    console.log(animalList);
  } else {
    let animalList = readFile("animalList.json");
    console.log(animalList);
  }

  fs.writeFileSync("animalList.json", JSON.stringify(animalList));
};

const eat = (animal, food) => {
  var index = animalList.findIndex((x) => x.animal === animal);

  if (index === -1) {
    console.log("the animal doesnt exist");
    console.log(animalList);
  } else {
    if (animal === "tiger") {
      let t = new Tiger(food, animal);
      t.eat();
    } else if (animal === "monkey") {
      let m = new Monkey(food, animal);
      m.eat();
    }
  }
  fs.writeFileSync("animalList.json", JSON.stringify(animalList));
};

const play = (animal) => {
  let index = animalList.findIndex((x) => x.animal === animal);

  if (index === -1) {
    console.log("the animal doesnt exist");
    console.log(animalList);
  } else {
    if (animal === "tiger") {
      let t = new Tiger("", animal);
      t.play();
    } else if (animal === "monkey") {
      let m = new Monkey("", animal);
      m.play();
    }
  }
  fs.writeFileSync("animalList.json", JSON.stringify(animalList));
};

const sleep = (animal) => {
  let index = animalList.findIndex((x) => x.animal === animal);

  if (index === -1) {
    console.log("the animal doesnt exist");
    console.log(animalList);
  } else {
    if (animal === "tiger") {
      let t = new Tiger("", animal);
      t.sleep();
    } else if (animal === "monkey") {
      let m = new Monkey("", animal);
      m.sleep();
    }
  }
  fs.writeFileSync("animalList.json", JSON.stringify(animalList));
};

const sound = (animal) => {
  let index = animalList.findIndex((x) => x.animal === animal);

  if (index === -1) {
    console.log("the animal doesnt exist");
    console.log(animalList);
  } else {
    if (animal === "tiger") {
      let t = new Tiger("", animal);
      t.sound();
    } else if (animal === "monkey") {
      let m = new Monkey("", animal);
      m.sound();
    }
  }
  fs.writeFileSync("animalList.json", JSON.stringify(animalList));
};

const printList = (animalType) => {
  let animalList = readFile("animalList.json");
  if (animalType === undefined) {
    console.log(animalList);
  }
};

const instructions = () => {
  console.log("Welcome to animals zoo :)");
  console.log(
    "The script use variables for example if you want to make a monkey eat"
  );
  console.log("node app eat --animal=monkey --food=grains");
  console.log("node app sleep --animal=monkey");
  console.log("For show the list of animals and their energy");
  console.log("node app list");

  console.log("Happy coding (:");
};

module.exports = {
  add,
  eat,
  play,
  sleep,
  sound,
  printList,
  instructions,
};
